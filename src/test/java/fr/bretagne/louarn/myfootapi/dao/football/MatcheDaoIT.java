package fr.bretagne.louarn.myfootapi.dao.football;

import fr.bretagne.louarn.myfootapi.IntegrationTest;
import fr.bretagne.louarn.myfootapi.dao.football.modele.MatchesResponse;
import fr.bretagne.louarn.myfootapi.exeption.DaoException;
import fr.bretagne.louarn.myfootapi.exeption.football.FootballDaoException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

import static org.junit.Assert.assertTrue;

public class MatcheDaoIT extends IntegrationTest {

    @Autowired
    private IMatcheDao matcheDao;

    private static String ligue1Code = "FL1";

    private static String invalidCode = "invalidCode";

    private static FootballDaoException footballDaoExeption = FootballDaoException
            .builder()
            .errorCode(FootballDaoException.Errors.ERROR_400.getCode())
            .build();

    @Test
    public void shouldSearcheMatches() throws FootballDaoException, DaoException {
        MatchesResponse matchesResponse = matcheDao.searcheMatches(ligue1Code);

        assertTrue(Objects.nonNull(matchesResponse));
    }

    @Test
    public void shouldNotSearcheMatches() {
        Assertions.assertThatThrownBy(() -> matcheDao.searcheMatches(invalidCode))
                .isInstanceOf(FootballDaoException.class)
                .isEqualTo(footballDaoExeption);
    }

}
