package fr.bretagne.louarn.myfootapi.dao.football;

import fr.bretagne.louarn.myfootapi.IntegrationTest;
import fr.bretagne.louarn.myfootapi.dao.football.modele.Player;
import fr.bretagne.louarn.myfootapi.exeption.DaoException;
import fr.bretagne.louarn.myfootapi.exeption.football.FootballDaoException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PlayerDaoIT extends IntegrationTest {

    @Autowired
    IPlayerDao playerDao;

    private static Integer playerId = 54;

    private static Integer randomPlayerId = 999999999;

    private static FootballDaoException footballDaoExeption = FootballDaoException
            .builder()
            .error(FootballDaoException.Errors.ERROR_500.getCode())
            .build();

    @Test
    public void shouldFindPlayerById() throws FootballDaoException, DaoException {
        Assertions.assertThat(playerDao.findPlayerById(playerId))
                .extracting(Player::getId)
                .isEqualTo(playerId);
    }

    @Test
    public void shouldNotFindPlayerById() {
        Assertions.assertThatThrownBy(() -> playerDao.findPlayerById(randomPlayerId))
                .isInstanceOf(FootballDaoException.class)
                .isEqualTo(footballDaoExeption);
    }
}
