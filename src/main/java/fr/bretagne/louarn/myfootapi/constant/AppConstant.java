package fr.bretagne.louarn.myfootapi.constant;

public class AppConstant {

    private AppConstant() {
    }

    /**
     * Nom de la table de sequance.
     */
    public static final String SEQUENCE = "sequence";

}
