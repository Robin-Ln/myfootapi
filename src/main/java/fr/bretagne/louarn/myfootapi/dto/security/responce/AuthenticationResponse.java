package fr.bretagne.louarn.myfootapi.dto.security.responce;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class AuthenticationResponse implements Serializable {

    /**
     * Token d'authentification.
     */
    private String jwt;

}
