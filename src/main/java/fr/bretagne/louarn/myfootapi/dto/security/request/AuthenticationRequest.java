package fr.bretagne.louarn.myfootapi.dto.security.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class AuthenticationRequest implements Serializable {

    /**
     * Login de l'utilisateyr.
     */
    private String username;

    /**
     * Mot de passe de l'utilisateur.
     */
    private String password;
}
