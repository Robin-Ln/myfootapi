package fr.bretagne.louarn.myfootapi.dto.sigin.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SiginRequest {

    /**
     * Login utilisateur.
     */
    private String username;

    /**
     * Mot de passe utilisateur.
     */
    private String password;
}
