package fr.bretagne.louarn.myfootapi.config.properties;

import lombok.Getter;
import lombok.Setter;
import okhttp3.logging.HttpLoggingInterceptor;


@Getter
@Setter
public class ApiProperties {

    /**
     * Url de l'api.
     */
    private String basePath;

    /**
     * Clé d'acces à l'api.
     */
    private String apiKey;

    /**
     * Niveau de log.
     */
    private HttpLoggingInterceptor.Level level;
}
