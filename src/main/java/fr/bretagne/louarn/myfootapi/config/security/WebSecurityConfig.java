package fr.bretagne.louarn.myfootapi.config.security;

import fr.bretagne.louarn.myfootapi.filters.JwtRequestFilter;
import fr.bretagne.louarn.myfootapi.service.security.IUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Service d'authrntification de l'utilisateur.
     */
    private IUserDetailsService userDetailsService;

    /**
     * Filtre d'analyse du token.
     */
    private JwtRequestFilter jwtRequestFilter;

    /**
     * Encoder de mot de passe.
     */
    private PasswordEncoder passwordEncoder;

    /**
     * Constructeur avec paramètre.
     *
     * @param userDetailsService Service d'authrntification de l'utilisateur
     * @param jwtRequestFilter   Filtre d'analyse du token
     * @param passwordEncoder    Encoder de mot de passe
     */
    @Autowired
    public WebSecurityConfig(IUserDetailsService userDetailsService,
                             JwtRequestFilter jwtRequestFilter,
                             PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.jwtRequestFilter = jwtRequestFilter;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Configuration du service d'authentification et de l'encodage du password.
     *
     * @param auth builder de l'authentication manager
     * @throws Exception Exception lors de la création du builder
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/swagger.json",
                        "/actuator/**");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // Configuration csrf
        httpSecurity.csrf().disable();

        // Autentification
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

        // Pas d'Autentification
        httpSecurity.authorizeRequests(authorizeRequests ->
                authorizeRequests
                        .antMatchers("/security/authenticate", "/sigin").permitAll()
                        .anyRequest().authenticated()
        );

        httpSecurity.exceptionHandling();

        httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

}
