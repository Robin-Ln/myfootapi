package fr.bretagne.louarn.myfootapi.dao.football;

import fr.bretagne.louarn.myfootapi.dao.football.modele.Player;
import fr.bretagne.louarn.myfootapi.exeption.DaoException;
import fr.bretagne.louarn.myfootapi.exeption.football.FootballDaoException;

public interface IPlayerDao {
    /**
     * Recherche d'un joueur en fonction de son id.
     *
     * @param id identifiant du joueur
     * @return un joureur
     * @throws DaoException         exception dans la couche DAO
     * @throws FootballDaoException exception lors de l'appelle vers l'api
     */
    Player findPlayerById(Integer id) throws DaoException, FootballDaoException;
}
