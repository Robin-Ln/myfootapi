package fr.bretagne.louarn.myfootapi.dao.football.ressource;

import fr.bretagne.louarn.myfootapi.dao.football.modele.MatchesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MatcheRessource {


    /**
     * Recheche les matchs d'une compétition.
     *
     * @param competition nom de la compétition
     * @return Appelle qui retourne les matchs de la compétition
     */
    @GET(value = "matches")
    Call<MatchesResponse> searcheMatches(@Query("competitions") String competition);


}
