package fr.bretagne.louarn.myfootapi.dao.football.modele;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Filters {

    /**
     * Id.
     */
    private Integer id;

    /**
     * Matchday.
     */
    private Integer matchday;

    /**
     * status.
     */
    private Status status;

    /**
     * venue.
     */
    private Venue venue;

    /**
     * dateFrom.
     */
    private LocalDate dateFrom;

    /**
     * dateTo.
     */
    private LocalDate dateTo;

    /**
     * stage.
     */
    private String stage;

    /**
     * season.
     */
    private LocalDate season;

    /**
     * permission.
     */
    private String permission;

    /**
     * plan.
     */
    private String plan;

    /**
     * limit.
     */
    private String limit;
}
