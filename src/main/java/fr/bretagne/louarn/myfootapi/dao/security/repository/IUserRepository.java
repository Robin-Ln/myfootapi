package fr.bretagne.louarn.myfootapi.dao.security.repository;

import fr.bretagne.louarn.myfootapi.modele.security.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<MyUser, Integer> {

    /**
     * Recherche par nom.
     * @param username nom
     * @return utilisateur
     */
    Optional<MyUser> findUserByUsername(String username);

    /**
     * Recherche par nom.
     * @param username nom
     * @return vrai si utilisateur existe
     */
    boolean existsByUsername(String username);

}
