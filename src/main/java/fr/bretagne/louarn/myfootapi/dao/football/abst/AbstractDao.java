package fr.bretagne.louarn.myfootapi.dao.football.abst;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.bretagne.louarn.myfootapi.exeption.DaoException;
import lombok.Getter;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Optional;

public abstract class AbstractDao<T> {

    /**
     * Permet de mapper un json en objet.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Implémentation d'un ressource Retrofit.
     */
    @Getter
    private T ressource;


    /**
     * Récupération de la config Retrofit.
     *
     * @return Config Retrofit
     */
    protected abstract Retrofit getRetrofit();

    /**
     * Récupération du type de la ressource Retrofit.
     *
     * @return Class de la ressource
     */
    protected abstract Class<T> getClazz();

    /**
     * Permet la récupération de la configuration en fonction du DAO.
     */
    @PostConstruct
    public void init() {
        ressource = getRetrofit().create(getClazz());
    }


    /**
     * Permet l'éxecution d'un appelle Retrofit.
     *
     * @param call Appelle Retrofit
     * @param <R>  Type de retoure d'un applle Retrofit
     * @return Retourne la réponse d'un appelle Retrofit
     * @throws DaoException Execption associer à un appelle retrofit
     */
    public <R> Response<R> execute(Call<R> call) throws DaoException {
        try {
            return call.execute();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Permet de mapper la réponse d'un appelle Rétrofit en erreur.
     *
     * @param errorBody Erreur de l'appelle Retrofit
     * @param clazz     Class de l'exeption à mapper
     * @param <E>       Type de l'exeption à mapper
     * @return L'exeption associer à l'appelle Retrofit
     */
    public <E> Optional<E> getErrorBody(ResponseBody errorBody, Class<E> clazz) {
        try {
            String json = errorBody.string();
            E e = objectMapper.readValue(json, clazz);
            return Optional.ofNullable(e);
        } catch (IOException ex) {
            return Optional.empty();
        }
    }

}
