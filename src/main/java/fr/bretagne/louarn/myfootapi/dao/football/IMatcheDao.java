package fr.bretagne.louarn.myfootapi.dao.football;

import fr.bretagne.louarn.myfootapi.dao.football.modele.MatchesResponse;
import fr.bretagne.louarn.myfootapi.exeption.DaoException;
import fr.bretagne.louarn.myfootapi.exeption.football.FootballDaoException;

public interface IMatcheDao {

    /**
     * Recheche les matchs d'une compétition.
     *
     * @param competition nom de la compétition
     * @return Les matchs de la compétition
     * @throws DaoException         exception dans la couche DAO
     * @throws FootballDaoException exception lors de l'appelle vers l'api
     */
    MatchesResponse searcheMatches(String competition) throws DaoException, FootballDaoException;
}
