package fr.bretagne.louarn.myfootapi.dao.football.abst;

import fr.bretagne.louarn.myfootapi.exeption.football.FootballDaoException;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import retrofit2.Response;
import retrofit2.Retrofit;

public abstract class AbstractFootballDao<T> extends AbstractDao<T> {

    /**
     * Injection de la configuration Retrofit.
     */
    @Autowired
    private Retrofit retrofit;


    @Override
    protected Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * Permet la récupération de la réponse de l'appelle.
     *
     * @param response Réponse de l'appelle Retrofit.
     * @param <R>      Type de la réponse
     * @return Réponse de l'appelle rétrofit
     * @throws FootballDaoException Exeption en cas d'echec de l'appelle
     */
    public <R> R getOrElseThrow(Response<R> response) throws FootballDaoException {
        if (BooleanUtils.isFalse(response.isSuccessful())
                && response.errorBody() != null) {
            throw getErrorBody(response.errorBody(), FootballDaoException.class)
                    .orElseThrow(FootballDaoException::new);
        }
        return response.body();
    }

}
