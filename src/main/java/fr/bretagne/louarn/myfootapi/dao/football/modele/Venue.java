package fr.bretagne.louarn.myfootapi.dao.football.modele;

public enum Venue {
    /**
     * HOME.
     */
    HOME,

    /**
     * AWAY.
     */
    AWAY;
}
