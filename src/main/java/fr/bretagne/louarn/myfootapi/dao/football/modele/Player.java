package fr.bretagne.louarn.myfootapi.dao.football.modele;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Player {
    /**
     * id.
     */
    private Integer id;

    /**
     * name.
     */
    private String name;

    /**
     * firstName.
     */
    private String firstName;

    /**
     * lastName.
     */
    private String lastName;

    /**
     * dateOfBirth.
     */
    private LocalDate dateOfBirth;

    /**
     * countryOfBirth.
     */
    private String countryOfBirth;

    /**
     * nationality.
     */
    private String nationality;

    /**
     * position.
     */
    private String position;

    /**
     * lastUpdated.
     */
    private LocalDateTime lastUpdated;
}
