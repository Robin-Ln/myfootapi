package fr.bretagne.louarn.myfootapi.dao.football.modele;

public enum Status {
    /**
     * SCHEDULED.
     */
    SCHEDULED,
    /**
     * LIVE.
     */
    LIVE,

    /**
     * IN_PLAY.
     */
    IN_PLAY,

    /**
     * PAUSED.
     */
    PAUSED,

    /**
     * FINISHED.
     */
    FINISHED,

    /**
     * POSTPONED.
     */
    POSTPONED,

    /**
     * SUSPENDED.
     */
    SUSPENDED,

    /**
     * CANCELED.
     */
    CANCELED;
}
