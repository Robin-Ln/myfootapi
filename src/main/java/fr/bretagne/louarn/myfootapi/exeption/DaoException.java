package fr.bretagne.louarn.myfootapi.exeption;

public class DaoException extends Exception {

    /**
     * Constructeur avec paramètre.
     *
     * @param cause Cause de l'exception
     */
    public DaoException(Throwable cause) {
        super(cause);
    }
}
