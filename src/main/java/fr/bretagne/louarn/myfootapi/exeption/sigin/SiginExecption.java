package fr.bretagne.louarn.myfootapi.exeption.sigin;

public class SiginExecption extends RuntimeException {

    /**
     * Constructeur avec paramètre.
     *
     * @param message message de l'exeption
     */
    public SiginExecption(String message) {
        super(message);
    }
}
