package fr.bretagne.louarn.myfootapi.exeption.football;


import lombok.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = false)
public class FootballDaoException extends Exception {

    /**
     * Code d'une erreur technique.
     */
    private final Integer error;

    /**
     * Code d'une erreur fonctionnelle.
     */
    private final Integer errorCode;

    /**
     * Constructeur sans paramètre.
     */
    public FootballDaoException() {
        super();
        this.error = null;
        this.errorCode = null;
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param error Code d'une erreur
     */
    public FootballDaoException(Integer error) {
        super();
        this.error = error;
        this.errorCode = error;
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param error     Code d'une erreur technique
     * @param errorCode Code d'une erreur fonctionnelle
     */
    public FootballDaoException(Integer error, Integer errorCode) {
        super();
        this.error = error;
        this.errorCode = errorCode;
    }

    @Getter
    @AllArgsConstructor
    public enum Errors {
        /**
         * erreur avec le code 400.
         */
        ERROR_400(400),

        /**
         * erreur avec le code 500.
         */
        ERROR_500(500);

        /**
         * Code de l'erreur.
         */
        private Integer code;

    }
}
