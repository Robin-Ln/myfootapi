package fr.bretagne.louarn.myfootapi.controller.sigin;

import fr.bretagne.louarn.myfootapi.dto.sigin.request.SiginRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Controller d'enregistrement")
public interface IUserController {

    /**
     * Permet l'inscription d'un utilisateur.
     *
     * @param request Requete d'inscription
     */
    @ApiOperation(value = "inscription")
    void sigin(SiginRequest request);

}
