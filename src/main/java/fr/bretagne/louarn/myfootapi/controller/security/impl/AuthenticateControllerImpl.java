package fr.bretagne.louarn.myfootapi.controller.security.impl;

import fr.bretagne.louarn.myfootapi.controller.security.IAuthenticateController;
import fr.bretagne.louarn.myfootapi.dto.security.request.AuthenticationRequest;
import fr.bretagne.louarn.myfootapi.dto.security.responce.AuthenticationResponse;
import fr.bretagne.louarn.myfootapi.service.security.IAuthenticateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/security")
public class AuthenticateControllerImpl implements IAuthenticateController {

    /**
     * Service d'authentification.
     */
    @Autowired
    private IAuthenticateService authenticateService;

    @Override
    @PostMapping(value = "/authenticate")
    public AuthenticationResponse createAuthenticationToken(@RequestBody AuthenticationRequest request) {
        return authenticateService.createAuthenticationToken(request);
    }

}
