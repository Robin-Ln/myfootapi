package fr.bretagne.louarn.myfootapi.controller.security;

import fr.bretagne.louarn.myfootapi.dto.security.request.AuthenticationRequest;
import fr.bretagne.louarn.myfootapi.dto.security.responce.AuthenticationResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Controller d'authentification")
public interface IAuthenticateController {

    /**
     * Permet l'authentification d'un utilisateur.
     *
     * @param request Requete d'authentification
     * @return un token d'authentification
     */
    @ApiOperation(value = "token")
    AuthenticationResponse createAuthenticationToken(AuthenticationRequest request);
}
