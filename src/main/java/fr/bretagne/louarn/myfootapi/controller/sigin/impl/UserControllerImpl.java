package fr.bretagne.louarn.myfootapi.controller.sigin.impl;

import fr.bretagne.louarn.myfootapi.controller.sigin.IUserController;
import fr.bretagne.louarn.myfootapi.dto.sigin.request.SiginRequest;
import fr.bretagne.louarn.myfootapi.service.sigin.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sigin")
public class UserControllerImpl implements IUserController {

    /**
     * Service utilisateur.
     */
    private IUserService userService;

    /**
     * Constructeur avec paramètre.
     *
     * @param userService Service utilisateur
     */
    @Autowired
    public UserControllerImpl(IUserService userService) {
        this.userService = userService;
    }

    @Override
    @PostMapping
    public void sigin(@RequestBody SiginRequest request) {
        userService.sigin(request);
    }
}
