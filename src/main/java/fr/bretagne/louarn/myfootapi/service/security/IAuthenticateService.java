package fr.bretagne.louarn.myfootapi.service.security;

import fr.bretagne.louarn.myfootapi.dto.security.request.AuthenticationRequest;
import fr.bretagne.louarn.myfootapi.dto.security.responce.AuthenticationResponse;

public interface IAuthenticateService {

    /**
     * Création d'un token.
     *
     * @param request Requete de création du token
     * @return Un token d'authentification
     */
    AuthenticationResponse createAuthenticationToken(AuthenticationRequest request);

}
