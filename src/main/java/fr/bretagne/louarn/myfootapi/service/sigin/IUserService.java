package fr.bretagne.louarn.myfootapi.service.sigin;

import fr.bretagne.louarn.myfootapi.dto.sigin.request.SiginRequest;

public interface IUserService {

    /**
     * Enregistrement d'un utilisateur.
     * @param request requete d'enregistrement
     */
    void sigin(SiginRequest request);

}
