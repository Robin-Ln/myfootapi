package fr.bretagne.louarn.myfootapi.service.security;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.function.Function;

public interface IJwtservice {

    /**
     * Récupération du login de l'utilisateur.
     *
     * @param token jwt d'authentification
     * @return login de l'utilisateur
     */
    String extractUsername(String token);

    /**
     * Récupération la date d'expiration du token.
     *
     * @param token jwt d'authentification
     * @return date d'expiration du token
     */
    Date extractExpiration(String token);

    /**
     * Permet l'extraction d'un élément du token.
     *
     * @param token          jwt d'authentification
     * @param claimsResolver Fonction qui mermet la conversion
     * @param <T>            Type du champ à récupérer
     * @return champ présent dans le token
     */
    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    /**
     * Création d'un token à partir d'un utilisateur.
     *
     * @param userDetails information de l'utilisateur
     * @return jwt d'authentification
     */
    String generateToken(UserDetails userDetails);

    /**
     * Vérifi la validiter du token.
     *
     * @param token       jwt d'authentification
     * @param userDetails information de l'utilisateur
     * @return vrai si le token est valide
     */
    Boolean validateToken(String token, UserDetails userDetails);
}
