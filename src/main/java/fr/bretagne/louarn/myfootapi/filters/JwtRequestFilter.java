package fr.bretagne.louarn.myfootapi.filters;

import fr.bretagne.louarn.myfootapi.service.security.IJwtservice;
import fr.bretagne.louarn.myfootapi.service.security.IUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    /**
     * Service de gestion de l'utilisateur.
     */
    private IUserDetailsService userDetailsService;

    /**
     * Service de gestion de token.
     */
    private IJwtservice jwtservice;

    /**
     * Prefix d'un token.
     */
    private static final String BEARER = "Bearer ";

    /**
     * Constructeur avec paramètre.
     *
     * @param userDetailsService Service de gestion de l'utilisateur
     * @param jwtservice         Service de gestion de token
     */
    @Autowired
    public JwtRequestFilter(
            IUserDetailsService userDetailsService,
            IJwtservice jwtservice) {
        this.userDetailsService = userDetailsService;
        this.jwtservice = jwtservice;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain)
            throws ServletException, IOException {

        final String authorizationHeader = request.getHeader("Authorization");

        String username = null;
        String jwt = null;

        if (authorizationHeader != null
                && authorizationHeader.startsWith(BEARER)) {
            jwt = authorizationHeader.substring(BEARER.length());
            username = jwtservice.extractUsername(jwt);
        }


        if (username != null
                && SecurityContextHolder.getContext()
                .getAuthentication() == null) {

            UserDetails userDetails = this.userDetailsService
                    .loadUserByUsername(username);

            if (jwtservice.validateToken(jwt, userDetails).equals(true)) {

                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        chain.doFilter(request, response);
    }

}
