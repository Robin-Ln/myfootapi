package fr.bretagne.louarn.myfootapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFootApiApplication {

    /**
     * Méthode de lancement de l'application
     * @param args arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(MyFootApiApplication.class);
    }

}
